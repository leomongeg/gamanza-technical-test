# ReactJS Web App - MVVM Development Stack - Gamanza Technical Test

Development Stack to build MVVM web applications with ReactJS. 

Features:

 - Mobx to handle the state management
 - Dependency Injection with TypeDI
 - Model serialization with serializr
 - Routes based on [configuration file](src/Routes/routes.ts)
 - Store Classes management
 - UI Layer with MaterialUI Component
 - Middleware Pattern
 - Pre-Configured Axios & Axios interceptors
 

## Requerimientos:

 - NodeJS 14.15.0 (npm v6.14.8)
 - [Yarn](https://yarnpkg.com/en/)
 - [nvm](https://github.com/nvm-sh/nvm) [Optional] 


## Installation

1- Clone/download the repo: 

```bash
 $ git clone https://bitbucket.org/leomongeg/gamanza-technical-test.git
```

2- Install dependencies:

```bash
 $ yarn install
````

3- Create the env file. Please create the env file base on [.env.example](.env.example)

```bash
 $ cp .env.example .env
```

## Usage

**Development**

For the development environment, the project has a node script that is in charge of 
doing the hot-reaload and compilation tasks. You just have to run
 
```bash
 $ yarn run start-dev
``` 

* Build app continuously (HMR enabled)
* App served @ `http://localhost:8080`

**Production**

To build the project for production run:

```bash
 $ yarn run build
```

* Build app once (HMR disabled) to `/dist/`
* App served @ `http://localhost:3000`

---

**All commands**

Command | Description
--- | ---
`yarn run start-dev` | Build app continuously (HMR enabled) and serve @ `http://localhost:8080`
`yarn run start-prod` | Build app once (HMR disabled) to `/dist/` and serve @ `http://localhost:3000`
`yarn run build` | Build app to `/dist/`
`yarn run test` | Run tests
`yarn run lint` | Run Typescript linter
`yarn run start` | (alias of `yarn run start-dev`)

**Note**: replace `yarn` with `npm` if you use npm.


## Project architecture and motivation

A couple of years ago I had to develop a project skeleton and architecture to develop React apps. Taking my experience 
working on Backend and full-stack mobile frameworks on iOS and Android, take the MVVM design pattern to build ReactJS 
web applications to help create a well-structured project to easy allow testability with unit testing using concepts 
like services (dependency injection) and isolate the UI (React side) to build only the UI components.

The project has other features to handle users and authentication as well roles and permission (just in case) you see
other files with extra functionality.

For this technical test, I created two kinds of components. 

For the Countries (landing page) I use class components:

 - [src/ViewModel/Pages/HomeViewModel/index.tsx](src/ViewModel/Pages/HomeViewModel/index.tsx) This is the view model for 
 the home landing page, this contains the business logic that connects the view with the model/store.
 - [src/ViewModel/Pages/HomeViewModel/view.tsx](src/ViewModel/Pages/HomeViewModel/view.tsx) This contains the actual 
 view the visual components and styling.

For the Universities I use functional components with React Hooks:

 - [src/ViewModel/Pages/UniversitiesViewModel/index.tsx](src/ViewModel/Pages/UniversitiesViewModel/index.tsx) This 
 component handles the view model and view.

There are other components to handle some utilitarian features like table actions and filters as well as to handle the 
layout and headers.

![Countries](./Screen-Shot-2021-09-02-at 22.54.20.png)

![Universities](./Screen-Shot-2021-09-02-at-22.58.27.png)

## See also
* [React Webpack Babel Starter](https://github.com/vikpe/react-webpack-babel-starter)
* [Isomorphic Webapp Starter](https://github.com/vikpe/isomorphic-webapp-starter)
