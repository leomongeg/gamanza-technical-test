import { match }  from "react-router";
import * as React from "react";

export default abstract class BaseMiddleware {
    private match: match<any>;

    /**
     * Validate abstract function
     *
     * @param options
     */
    abstract validate(options?: any): boolean ;

    /**
     * Reject abstract function
     *
     * @param props
     */
    abstract reject(props?: any): React.ReactNode ;

    /**
     * get match function
     */
    public getMatch(): match<any> {
        return this.match;
    }

    /**
     * Set match function
     *
     * @param value
     */
    public setMatch(value: match) {
        this.match = value;
    }

    /**
     * Get name function
     */
    public static getName(): string {
        throw "You need implement the function static getAlias on your middleware.";
    }
}