export const paths = {
    services: [
        "/services",
        "/maintenance",
        "/appointments",
        "/credits",
        "/insurance"
    ],
    vehicles: [
        "/history-details/",
        "/vehicle/history/",
        "/vehicle/",
        "/vehicles",
        "/add-vehicle",
    ],
    help: [
        "/help",
        "/tips",
        "/faq",
    ]
};