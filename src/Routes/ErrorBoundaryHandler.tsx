import * as React             from "react";
import { Container }          from "typedi";
import LoggerReportingService from "../Service/LoggerReportingService";

export class ErrorBoundaryHandler extends React.Component<any, any> {
    private loggerService: LoggerReportingService;

    constructor(props: any) {
        super(props);
        this.state         = {hasError: false};
        this.loggerService = Container.get(LoggerReportingService);

        // Handle Global runtime errors
        window.addEventListener("unhandledrejection", function (promiseRejectionEvent: PromiseRejectionEvent) {
            if (promiseRejectionEvent.reason) {
                const loggerService = Container.get(LoggerReportingService);
                loggerService.error(promiseRejectionEvent.reason.message || "Uncaught Exception",
                                    promiseRejectionEvent.reason.stack || JSON.stringify(promiseRejectionEvent.reason.stack),
                                    `unhandledrejection Event Handler`)
                             .service.dispatchLogs();
            }
        });
    }

    /**
     * React Life cycle event.
     *
     * @param error
     * @param info
     */
    componentDidCatch(error: Error, info: React.ErrorInfo) {
        // Display fallback UI
        // this.setState({hasError: true});
        // You can also log the error to an error reporting service
        // logErrorToMyService(error, info);
        this.setState({hasErrors: true});
        this.loggerService.error(error.message || "Uncaught Exception",
                                 error.stack || JSON.stringify(error),
                                 `ComponentDidCatch Event Handler`,
                                 [{componentStack: JSON.stringify(info)}])
            .service.dispatchLogs();
    }

    /**
     * React life cycle event.
     */
    componentDidMount() {
        // Initialize the auto dispatch logs handler.
        this.loggerService.setupAutoDispatchLogs();
    }

    /**
     * React Component render
     */
    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <h1>Something went wrong.</h1>;
        }
        return this.props.children;
    }
}
