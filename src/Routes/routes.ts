import * as React            from "react";
import BaseMiddleware        from "../Middleware/Base/BaseMiddleware";
import HomeViewModel         from "../ViewModel/Pages/HomeViewModel";
import UniversitiesViewModel from "../ViewModel/Pages/UniversitiesViewModel";
import NotFoundViewModel     from "../ViewModel/Pages/NotFoundViewModel";


/**
 * Base interface for the route object
 */
interface Route {
    auth?: boolean;
    component: React.ComponentType;
    exact?: boolean;
    layout?: React.ComponentType;
    middleware?: BaseMiddleware[];
    options?: any;
    name: string;
    path: string;
}

const routesList: Route[] = [
    {
        auth     : false,
        component: HomeViewModel,
        exact    : true,
        name     : "home-page",
        path     : "/",
    },
    {
        auth     : false,
        component: UniversitiesViewModel,
        exact    : true,
        name     : "universities",
        path     : "/:code/universities",
    },
    {
        auth     : false,
        component: NotFoundViewModel,
        exact    : true,
        name     : "not-found",
        path     : "/404",
    }
];

export default routesList;
