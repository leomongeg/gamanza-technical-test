import * as React from "react";
import { prefix } from "../Utils/prefix";

/**
 * Inject prefix decoreator.
 *
 * @param COMPONENT
 * @constructor
 */
export default function InjectPrefix<T extends React.ComponentClass>(COMPONENT: T): T {
    return ((props: any) => <COMPONENT prefix={prefix} {...props} />) as any as T;
}
