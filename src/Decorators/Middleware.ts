import { Container }                from "typedi";
import { MiddlewareHandlerService } from "../Service/MiddlewareHandlerService";

/**
 * Middleware decorator.
 *
 * @param constructor
 * @constructor
 */
export default function Middleware(constructor: any) {
    const instance = new constructor();

    Container.get(MiddlewareHandlerService).registerMiddleware(constructor.getAlias(), instance);

    return constructor;
}