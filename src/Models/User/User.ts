import { observable } from "mobx";
import {
    serializable,
    alias,
    list,
    object
}                     from "serializr";
import { Role }       from "./Role";

export default class User {
    @serializable
    @observable
    private _id: string;

    @serializable(alias("idClient"))
    @observable
    public userId: string;

    @serializable
    @observable
    public name: string;

    @serializable
    @observable
    public lastName: string;

    @serializable
    @observable
    public email: string;

    @serializable(alias("roles", list(object(Role))))
    protected _roles: Role[] = [];

    /**
     * User id getter
     */
    public getId() {
        return this._id;
    }

    get roles(): Role[] {
        return this._roles;
    }

    set roles(value: Role[]) {
        this._roles = value;
    }

    /**
     * Check if User has role or almost one role in a list of roles
     * @param role
     */
    hasRole(roles: string | string[]): boolean {
        if (roles === "" || roles.length === 0) return true;
        if (!Array.isArray(roles)) {
            roles = [ roles ];
        }

        return this._roles.some((role: any) => roles.includes(role._name));
    }
}
