import { observable }                        from "mobx";
import { serializable, alias, deserialize }  from "serializr";

export default class File {
    @serializable
    @observable
    private fileId: string;

    @serializable(alias("secure_url", true))
    @observable
    private secureUrl: string;

    @serializable(alias("resource_type", true))
    @observable
    private resourceType: string;

    @serializable
    @observable
    private bytes: number;

    @serializable(alias("asset_id", true))
    @observable
    private assetId: string;

    @serializable
    @observable
    private format: string;

    @serializable
    @observable
    private height: number;

    @serializable(alias("public_id", true))
    @observable
    private publicId: string;

    @serializable
    @observable
    private version: number;

    @serializable(alias("version_id", true))
    @observable
    private versionId: string;

    @serializable
    @observable
    private width: number;

    /**
     * Getter
     */
    public getFileId(): string {
        return this.fileId;
    }

    /**
     * Setter
     *
     * @param val
     */
    public setFileId(val: string): this {
        this.fileId = val;
        return this;
    }

    /**
     * Getter
     */
    public getUrl(): string {
        return this.secureUrl;
    }

    /**
     * Setter
     *
     * @param val
     */
    public setUrl(val: string): this {
        this.secureUrl = val;
        return this;
    }

    /**
     * Getter
     */
    public getType(): string {
        return this.resourceType;
    }

    /**
     * Setter
     *
     * @param val
     */
    public setType(val: string): this {
        this.resourceType = val;
        return this;
    }

    /**
     * Getter
     */
    public getFileSize(): number {
        return this.bytes;
    }

    /**
     * Setter
     *
     * @param val
     */
    public setFileSize(val: number): this {
        this.bytes = val;
        return this;
    }

    constructor(values: object) {
        Object.assign(this, values);
    }

    /**
     * Getter
     */
    public getPublicId(): string {
        return this.publicId;
    }

    /**
     * Getter
     */
    public getResourceType(): string {
        return this.resourceType;
    }

    /**
     * Getter
     */
    public getVersion(): number {
        return this.version;
    }

    /**
     * Getter
     */
    public getFormat(): string {
        return this.format;
    }

    /**
     * Convert plain object data to Vehicle model.
     *
     * @param rawData
     */
    public static deserializeEntity(rawData: any): File[] {
        if (!Array.isArray(rawData)) {
            rawData = [rawData];
        }
        const hydrated: File[] = [];
        rawData.map((plainObj: any) => {
            const entity: File = deserialize(File, plainObj);
            hydrated.push(entity);
        });

        return hydrated;
    }

}