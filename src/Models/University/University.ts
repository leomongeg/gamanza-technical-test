import Country        from "../Country/Country";
import {
    alias,
    deserialize,
    list,
    primitive,
    serializable
}                     from "serializr";
import { observable } from "mobx";
import { v4 }           from "uuid";

export default class University {
    private _country: Country;

    @observable
    private _id = v4();

    @serializable(alias("state-province", true))
    @observable
    private _state: string;

    @serializable(alias("name", true))
    @observable
    private _name: string;

    @serializable(alias("domains", list(primitive())))
    @observable
    private _domains: string[];

    @serializable(alias("web_pages", list(primitive())))
    @observable
    private _webPage: string[];

    get id(): string {
        return this._id;
    }

    get country(): Country {
        return this._country;
    }

    get state(): string {
        return this._state;
    }

    get name(): string {
        return this._name;
    }

    get domains(): string[] {
        return this._domains;
    }

    get webPage(): string[] {
        return this._webPage;
    }

    /**
     * Convert plain object data to Country model.
     *
     * @param rawData
     */
    public static deserializeEntity(rawData: any, country: Country): University[] {
        if (!Array.isArray(rawData)) {
            rawData = [ rawData ];
        }
        const hydrated: University[] = [];
        rawData.map((plainObj: any) => {
            const entity: University = deserialize(University, plainObj);
            entity._country          = country;
            hydrated.push(entity);
        });

        return hydrated;
    }
}
