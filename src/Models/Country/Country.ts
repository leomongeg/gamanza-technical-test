import {
    alias,
    deserialize,
    serializable
} from "serializr";
import { observable }   from "mobx";

export default class Country {
    @serializable(alias("code", true))
    @observable
    private _code: string;

    @serializable(alias("name", true))
    @observable
    private _name: string;

    get code(): string {
        return this._code;
    }

    get name(): string {
        return this._name;
    }

    /**
     * Convert plain object data to Country model.
     *
     * @param rawData
     */
    public static deserializeEntity(rawData: any): Country[] {
        if (!Array.isArray(rawData)) {
            rawData = [rawData];
        }
        const hydrated: Country[] = [];
        rawData.map((plainObj: any) => {
            const entity: Country = deserialize(Country, plainObj);
            hydrated.push(entity);
        });

        return hydrated;
    }
}
