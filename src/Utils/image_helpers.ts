import File from "../Models/File/File";

/**
 * converts dataURI to image blob for usage as form data
 * @param dataURI string that looks like this: data:image/png;base64,iVBORw0KGgoAAA...
 */
export function ImageDataToBlob(dataURI: string) {
    const splitDataURI = dataURI.split(",");
    const byteString = splitDataURI[0].indexOf("base64") >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1]);
    const mimeString = splitDataURI[0].split(":")[1].split(";")[0];

    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++)
        ia[i] = byteString.charCodeAt(i);

    return new Blob([ia], { type: mimeString });
  }

/**
 * Transforms image based on Cloudinary Transformations
 * @param file File object
 * @param transformation Transformation title from Transformations (ex: Transformations.Thumbnail150)
 * @param defaultImg Default image absolute path
 */
export function image_url(file: File | undefined, transformation?: Transformations, defaultImg?: string) {
    const cloudURL = process.env.REACT_APP_CLOUDINARY;
    const transf = transformation ? `${transformations[transformation]},f_auto,q_auto:good/` : "f_auto,q_auto:good/";
    if (file == undefined) {
        return defaultImg || "/src/resources/images/logo.svg";
    }
    return `${cloudURL}/${file.getResourceType()}/upload/${transf}v${file.getVersion()}/${file.getPublicId()}`;
}

/**
 * converts dataURI to image blob for usage as form data
 * @param url Absolute url
 * @param transformation Transformation title from Transformations (ex: Transformations.Thumbnail150)
 */
export function ext_img_url(url: string, transformation?: Transformations) {
    const cloudURL = process.env.REACT_APP_CLOUDINARY;
    const transf = transformation ? `${transformations[transformation]},w_600,f_auto,q_auto:good/` : "f_auto,q_auto:good/";
    return `${cloudURL}/image/fetch/${transf}${url}`;
}

export enum Transformations {
    ProfileCircle = "ProfileCircle",
    Thumbnail150 = "Thumbnail150",
    Thumbnail300 = "Thumbnail300",
}

// To add more transformation strings check https://cloudinary.com/documentation/image_transformations
const transformations = {
    "ProfileCircle": "w_300,h_300,c_thumb,r_max,g_face,z_0.7",
    "Thumbnail150": "w_150,h_150,c_thumb",
    "Thumbnail300": "w_300,h_300,c_thumb",
};



