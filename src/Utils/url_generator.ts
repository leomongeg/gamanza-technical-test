import { ROUTING_CONFIG } from "../config/routing.config";
import { Container }      from "typedi";
import { RouterService }  from "../Service/RouterService";
import { generatePath }   from "react-router";

/**
 * Generate URL for the route name
 *
 * @param path
 */
export function url_for(name: string, params?: any): string {
    const path = Container.get(RouterService).getRoutePathByName(name);

    return generatePath(`${ROUTING_CONFIG.ROUTING_PREFIX + path}`, params);
}

/**
 * Generate the URL for the route path
 * @param name
 */
export function url_for_path(path: string) {
    return `${ROUTING_CONFIG.ROUTING_PREFIX + path}`;
}

/**
 * Generate full URL
 *
 * @param path
 */
export function url_full(path: string): string {
    return `//${window.location.host + ROUTING_CONFIG.ROUTING_PREFIX + path}`;
}