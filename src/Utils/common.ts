import { Container }          from "typedi";
import LoggerReportingService from "../Service/LoggerReportingService";

export const body = document.getElementsByTagName("body")[0];

/**
 * Convert all the first character of the given string uppercase.
 *
 * @param text
 */
export function ucFirst(text: string) {
    text = text.toLowerCase()
               .split(" ")
               .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
               .join(" ");

    return text;
}

/**
 * Convert only the first char of the given string to uppercase.
 *
 * @param text
 * @return {string}
 */
export function ucFirstOnly(text: string): string {
    return text.charAt(0).toUpperCase() + text.slice(1);
}

/**
 * Format the total amount
 *
 * @param amount
 */
export function formatAmount(amount: number): string {
    return parseFloat(amount.toString()).toFixed(2)
                                        .replace(/\d(?=(\d{3})+\.)/g, "$& ");
}

/**
 * Fix any accidental duplicated // in a generated url.
 *
 * @param url
 */
export function fixUrl(url: string) {
    return url.replace(/([^:]\/)\/+/g, "$1");
}

/**
 * Convert a string to Title Case (Fisrt letter of each word Uppercase)
 * @param baseString
 */
export function toTitleCase(baseString: string) {
    if (baseString.match(/[.,\(\)]/)) {
        return baseString;
    }
    const sentence = baseString.trim().toLowerCase().split(/ +/);
    for (let i = 0; i < sentence.length; i++) {
        sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }
    const newSentence = (sentence.join(" "));
    return newSentence;
}

/**
 * Extract ID from Youtube Link
 * @param url
 * http://www.youtube.com/watch?v=0zM3nApSvMg&feature=feedrec_grec_index
 * http://www.youtube.com/user/IngridMichaelsonVEVO#p/a/u/1/QdK8U-VIH_o
 * http://www.youtube.com/v/0zM3nApSvMg?fs=1&amp;hl=en_US&amp;rel=0
 * http://www.youtube.com/watch?v=0zM3nApSvMg#t=0m10s
 * http://www.youtube.com/embed/0zM3nApSvMg?rel=0
 * http://www.youtube.com/watch?v=0zM3nApSvMg
 * http://youtu.be/0zM3nApSvMg
 */
export function parseYoutubeLink(url: string) {
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    const match = url.match(regExp);
    const id = (match && match[7].length == 11) ? match[7] : "";
    return id;
}

/**
 * @description ### Returns Go / Lua like responses(data, err)
 * when used with await
 *
 * - Example response [ data, undefined ]
 * - Example response [ undefined, Error ]
 *
 *
 * When used with Promise.all([req1, req2, req3])
 * - Example response [ [data1, data2, data3], undefined ]
 * - Example response [ undefined, Error ]
 *
 *
 * When used with Promise.race([req1, req2, req3])
 * - Example response [ data, undefined ]
 * - Example response [ undefined, Error ]
 *
 * @param {Promise} promise
 * @returns {Promise} [ data, undefined ]
 * @returns {Promise} [ undefined, Error ]
 */
export const handle = (promise: Promise<any>) => {
    return promise
        .then(data => {
            if (data instanceof Error || data instanceof TypeError) {
                const loggerService = Container.get(LoggerReportingService);
                loggerService.error(data.message || "Exception In Handle Async Call",
                                    data.stack || JSON.stringify(data.stack),
                                    `commons.handle Async Handler`,
                                    [{
                                        "errorStructure": typeof (data as any).toJSON === "function" ? (data as any).toJSON() : ""
                                    }])
                             .service.dispatchLogs();
                return [undefined, data];
            }
            return [data, undefined];
        })
        .catch(error => {
            const loggerService = Container.get(LoggerReportingService);
            loggerService.error(error.message || "Exception In Handle Async Call",
                                error.stack || JSON.stringify(error.stack),
                                `commons.handle Async Handler`,
                                [{
                                    "errorStructure": typeof (error as any).toJSON === "function" ? (error as any).toJSON() : ""
                                }])
                         .service.dispatchLogs();
            Promise.resolve([undefined, error]);
            return [undefined, error];
        });
};


/**
 * Truncate string and add ellipsis
 * @param str string
 * @param n character length
 */
export function truncate(str: string, n: number) {
    return (str.length > n) ? str.substr(0, n - 1) + "..." : str;
}