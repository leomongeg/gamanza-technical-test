export type OembedProvider = {
    /**
     * Name of the provider
     */
    name: string;

    /**
     * Parse function that converts the current figure->oembed node into the real or expected fragment. If the function
     * return undefined, the component infers that the media url is unsupported by the provider.
     *
     * @param mediaUrl
     * @param node
     */
    parse: (mediaUrl: string, node?: string) => string | undefined;
};

export const providers: OembedProvider[] = [
    {
        name : "Youtube",
        parse: (mediaUrl: string, node?: string): string | undefined => {
            const match = mediaUrl.match(/^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);
            if (!(match && match[2].length == 11)) return undefined;

            return `<div class="ytb-cnt embed-media" style="padding:56.25% 0 0 0;position:relative;">
                        <iframe src="https://youtube.com/embed/${match[2]}"
                                frameborder="0"
                                scrolling="no"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                </div>`;
        }
    },
    {
        name : "Vimeo",
        parse: (mediaUrl: string, node?: string): string | undefined => {
            const match = mediaUrl.match(/\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_\-]+)?/i);
            if (!(match && match[1].length > 0)) return undefined;

            return `<div class="vmo-cnt embed-media" style="padding:56.25% 0 0 0;position:relative;">
                <iframe src="https://player.vimeo.com/video/${match[1]}"
                        frameBorder="0"
                        allow="autoplay; fullscreen"
                        allowFullScreen></iframe>
            </div>`;
        }
    }
];