import { Component, default as React } from "react";
import IBaseProps                      from "../../ViewModel/Props/IBaseProps";
import SimpleReactValidator            from "simple-react-validator";
import { withTranslation }             from "react-i18next";

/**
 * Component props definition
 */
interface SimpleReactValidatorLocaleLoaderProps extends IBaseProps {
    locale: string;
}

class SimpleReactValidatorLocaleLoader extends Component<SimpleReactValidatorLocaleLoaderProps, any> {

    constructor(props: SimpleReactValidatorLocaleLoaderProps) {
        super(props);
        const {locale, t} = this.props;
        // @ts-ignore addLocale not defined in typings but it is present in the JS Native API
        SimpleReactValidator.addLocale(locale, {
            numeric     : t("numeric"),
            min         : t("min"),
            max         : t("max"),
            required    : t("required"),
            alpha_num   : t("alpha_num"),
            alpha       : t("alpha"),
            email       :  t("email")
        });
    }

    /**
     * Component render
     */
    public render(): React.ReactNode {
        return this.props.children;
    }
}

export default withTranslation("validator")(SimpleReactValidatorLocaleLoader);