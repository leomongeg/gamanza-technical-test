/**
 * Array contains
 * @param arr
 * @param element
 * @param key
 */
export function contains<T extends any, K extends keyof T>(arr: T[], element: T, key?: K): T | undefined {
    return arr.find(item => {

        if (key && item.hasOwnProperty(key)) {
            return item[key] === element[key];
        }

        if (key && typeof item.key === "function") {
            return item.key() === element.key();
        }

        return item == element;
    });
}

/**
 * Update array item
 * @param arr
 * @param element
 * @param attr
 */
export function updateSaveItem<T, K extends keyof T>(arr: T[], element: T, attr?: K): T[] {
    const found: T | undefined = contains(arr, element, attr);

    if (!found) {
        arr.push(element);

        return arr;
    }

    const index = getIndex(arr, found);

    if (~index) arr[index] = element;

    return arr;

}

/**
 * Get array value at index
 * @param arr
 * @param element
 */
export function getIndex<T>(arr: T[], element: T) {
    return arr.indexOf(element);
}

/**
 * Remove array item
 * @param arr
 * @param element
 * @param key
 */
export function removeItem<T, K extends keyof T>(arr: T[], element: T, key?: K): T[] {
    const found: T | undefined = contains(arr, element, key);

    if (!found) {
        return arr;
    }

    const index = getIndex(arr, found);

    if (~index) {
        arr.splice(index, 1);
    }

    return arr;
}

