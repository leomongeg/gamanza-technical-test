import { Context }            from "serializr";
import { Container }          from "typedi";
import LoggerReportingService from "../../Service/LoggerReportingService";

export default {
    serializer  : (sourcePropertyValue: any): any => {
        // Convert the logs array object into a json string to be serialized
        return JSON.stringify(sourcePropertyValue);
    },
    deserializer: async (jsonValue: any,
                         callback: (err: any, targetPropertyValue: any) => void,
                         context: Context, currentPropertyValue: any): Promise<void> => {

        // Convert back the json string into a valid Log.information array of literal objects.
        if (typeof jsonValue === "string") {
            callback(undefined, JSON.parse(jsonValue));
        } else {
            const loggerService = Container.get(LoggerReportingService);
            loggerService.error("[ERROR] Uncaught Exception",
                                JSON.stringify(jsonValue),
                                `LoggerInformationPropSchema.deserializer`,
                                [
                                    {"detail": "jsonValue is not a string object"},
                                    {componentStack: JSON.stringify(jsonValue)}
                                ])
                         .service.dispatchLogs();
            callback(undefined, []);
        }
    }
};