export default {
    calendarDate : "MM-DD-YYYY",
    monthDay     : "MMM D",
    shortDate    : "MMM D, YY",
    shortDateYear: "MMM D, YYYY"
};