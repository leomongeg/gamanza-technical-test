export const APP_CONTAINER_ID = "root-webapp";

export default {
    /**
     * Default Material UI theme options
     * @url https://material-ui.com/customization/theming/
     * @url https://material-ui.com/customization/default-theme/#default-theme
     * @property {ThemeOptions}
     */
    themeOptions: {
        palette: {
            error  : {
                main: "#f44336"
            },
            type   : "dark",
            primary: {
                contrastText: "#fff",
                main        : "#4dabf5"
            }
        }
    },
    /**
     * Side menu options
     */
    menuOptions : {
        // Enable the tree view in the side menu, default disable
        treeMode: false
    }
};
