import * as React                       from "react";
import { Switch }                       from "react-router-dom";
import Routes                           from "../../../Routes/routes";
import Route                            from "../Route/Route";
import { ROUTING_CONFIG }               from "../../../config/routing.config";
import SimpleReactValidatorLocaleLoader from "../../../Utils/i18n/SimpleReactValidatorLocaleLoader";
import ThemeConfig                      from "../../../config/Theme";
import {
    createStyles,
    CssBaseline,
    withStyles,
    createMuiTheme,
    ThemeProvider
}                                       from "@material-ui/core";
import { ThemeOptions }                 from "@material-ui/core/styles/createMuiTheme";
import HeaderViewModel                  from "../../../ViewModel/Components/HeaderViewModel";
import NotFoundViewModel                from "../../../ViewModel/Pages/NotFoundViewModel";
import NotificationViewModel            from "../../../ViewModel/Components/NotificationViewModel";


class Layout extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
    }

    /**
     * render
     */
    public render(): React.ReactNode {
        const { classes } = this.props;
        return (
            <ThemeProvider theme={ theme }>
                <SimpleReactValidatorLocaleLoader locale="es">
                    <div className={ classes.root }>
                        <CssBaseline/>
                        <HeaderViewModel/>
                        <div className={ classes.content }>
                            <div className={ classes.toolbar }/>
                            <Switch>
                                { Routes.map((page, key) => {
                                    return (
                                        <Route
                                            exact={ page.exact }
                                            auth={ page.auth }
                                            path={ ROUTING_CONFIG.ROUTING_PREFIX + page.path }
                                            name={ page.name }
                                            key={ page.name }
                                            layout={ page!.layout }
                                            component={ page.component }
                                            middleware={ page.middleware }
                                            options={ page.options }
                                        />
                                    );
                                }) }
                                { <Route component={ NotFoundViewModel }/> }
                            </Switch>
                        </div>
                        <NotificationViewModel/>
                    </div>
                </SimpleReactValidatorLocaleLoader>
            </ThemeProvider>
        );
    }
}

/**
 * Customize the theme options here
 */
const theme = createMuiTheme(ThemeConfig.themeOptions as ThemeOptions);

const styles = createStyles((theme: any) => ({
    toolbar: theme.mixins.toolbar,
    root   : {
        display: "flex",
    },
    content: {
        flexGrow: 1,
        padding : theme.spacing(3),
    }
}));

export default withStyles(styles)(Layout);
