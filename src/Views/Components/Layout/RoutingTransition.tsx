import React, { Component, ReactNode } from "react";
import TransitionGroup                 from "react-transition-group/TransitionGroup";
import Transition                      from "react-transition-group/Transition";
import CSSTransition                   from "react-transition-group/CSSTransition";
import Routes                          from "../../../Routes/routes";
import Route                           from "../Route/Route";
import { ROUTING_CONFIG }              from "../../../config/routing.config";
import { Switch }                      from "react-router-dom";
import { withRouter }                  from "react-router";

class RoutingTransition extends Component<any, any> {
    /**
     * Component Render
     */
    public render(): ReactNode {
        const {location, state} = this.props;
        console.log(location, state);
        return (<TransitionGroup>
            <CSSTransition
                key={location.key}
                classNames="slide"
                timeout={300}
            >

                <Switch location={location}>
                    {Routes.map((page, key) => {
                        return (
                            <Route
                                exact={page.exact}
                                auth={page.auth}
                                path={ROUTING_CONFIG.ROUTING_PREFIX + page.path}
                                name={page.name}
                                key={page.name}
                                layout={page!.layout}
                                component={page.component}
                                middleware={page.middleware}
                                options={page.options}
                            />
                        );
                    })}
                </Switch>
            </CSSTransition>
        </TransitionGroup>);
    }
}

export default withRouter(RoutingTransition);