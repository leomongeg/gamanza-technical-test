import BaseStore      from "./Base/BaseStore";
import { observable } from "mobx";
import Country        from "../Models/Country/Country";
import { Container }  from "typedi";
import AxiosService   from "../Service/AxiosService";
import API_CONST      from "../config/API_CONST";
import {
    NotificationSeverity,
    NotificationStore
}                     from "./NotificationStore";

export class CountryStore extends BaseStore {
    public static readonly NAME_STORE: string = "CountryStore";

    /**
     * Implementation of the init function
     */
    protected init() {
        this.needPersistData = false;
    }

    @observable
    private _countriesDB: Country[] = [];

    @observable
    private _countries: Country[] = [];

    @observable
    private _isFetchingData: boolean = false;

    @observable
    private _hasErrors: boolean = false;

    @observable
    private _filterText: string = "";

    private _pageNumber: number = 0;

    private _pageSize: number = 10;

    @observable
    private _count: number = 0;

    private _isSortAsc: boolean = true;


    get countries(): Country[] {
        return this._countries;
    }

    get isFetchingData(): boolean {
        return this._isFetchingData;
    }

    get hasErrors(): boolean {
        return this._hasErrors;
    }

    get pageNumber(): number {
        return this._pageNumber;
    }

    get pageSize(): number {
        return this._pageSize;
    }

    get count(): number {
        return this._count;
    }

    get isSortAsc(): boolean {
        return this._isSortAsc;
    }

    get filterText(): string {
        return this._filterText;
    }

    /**
     * Fetch data from remote API and serialize to store.
     */
    public async fetchData(): Promise<void> {
        if (this._countriesDB.length > 0) return;
        this._isFetchingData   = true;
        const axios            = Container.get(AxiosService);
        const { status, data } = await axios.axios.get(API_CONST.GET_SEARCH);

        if (status !== 200) {
            this.applicationStore
                .getStore(NotificationStore)
                .showWithMessageAndSeverity("An error occurs trying to fetch countries from the remote API",
                                            NotificationSeverity.ERROR);
            this._isFetchingData = false;
            return;
        }

        const index        = new Set(); // This index allow handle the filter unique country in a performance way.
        const filteredData = (data as []).filter(({ alpha_two_code }: { alpha_two_code: string }) => {
            const key = `${ alpha_two_code }`;
            return !index.has(key) && index.add(key);
        }).map(({ alpha_two_code, country }: { alpha_two_code: string, country: string }) => ({
            name: country,
            code: alpha_two_code
        }));

        this._countriesDB = Country.deserializeEntity(filteredData);
        this.applySortingCriteria();
        this.updateDisplayData();
        this._isFetchingData = false;
    }

    /**
     * Apply the sort criteria to the main data collection.
     * @private
     */
    private applySortingCriteria() {
        this._countriesDB = this._countriesDB.slice().sort((a, b) =>
                                                               this.isSortAsc ?
                                                                   a.name.localeCompare(b.name) : b.name.localeCompare(a.name));
    }

    /**
     * Updates the store with the data to be displayed and notify to the view to render the changes.
     *
     * @private
     */
    private updateDisplayData() {
        const filtered  = this._countriesDB
                              .filter(country => this._filterText !== "" ? country.name.toLowerCase().includes(this._filterText.toLowerCase()) : true);
        this._countries = filtered.slice(this._pageNumber * this._pageSize, (this._pageNumber + 1) * this._pageSize);

        this._count = filtered.length;
    }

    /**
     * Define the new page number
     * @param pageNumber
     */
    public updatePage(pageNumber: number) {
        this._pageNumber = pageNumber;
        this.updateDisplayData();
    }

    /**
     * Update the page size
     * @param size
     */
    public updatePageSize(size: number) {
        this._pageSize   = size;
        this._pageNumber = 0;
        this.updateDisplayData();
    }

    /**
     * Change the sort criteria.
     */
    public changeSort() {
        this._isSortAsc = !this._isSortAsc;
        this.applySortingCriteria();
        this.updateDisplayData();
    }

    /**
     * Set the filter text and trigger the search.
     * @param text
     */
    public setFilterText(text: string) {
        this._filterText = text;
    }

    /**
     * Trigger the filters over the data.
     */
    public applyFilters() {
        this._pageNumber = 0;
        this.updateDisplayData();
    }

    /**
     * Clear the current filters.
     */
    public clearFilters() {
        this._filterText = "";
        this.applyFilters();
    }

    /**
     * Find a country by code
     *
     * @param refCode
     */
    public async findByCode(refCode: string): Promise<Country | undefined> {
        if (this._countriesDB.length === 0)
            await this.fetchData();
        return this._countriesDB.find(({ code }) => code === refCode.toUpperCase());
    }

}
