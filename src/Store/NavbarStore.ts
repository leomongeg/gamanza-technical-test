import BaseStore      from "./Base/BaseStore";
import { observable } from "mobx";

export class NavbarStore extends BaseStore {
    public static readonly NAME_STORE: string = "NavbarStore";

    @observable
    private _title: string = "";

    @observable
    private _showBackIcon: boolean = false;

    private _backCallback: any;

    /**
     * Implementation of the init function
     */
    protected init() {
        this.needPersistData = false;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get showBackIcon(): boolean {
        return this._showBackIcon;
    }

    get backCallback(): any {
        return this._backCallback;
    }

    /**
     * Display the back button in the navbar.
     */
    public enableBackButton() {
        this._showBackIcon = true;
    }

    /**
     * Display the back button in the navbar.
     */
    public disableBackButton() {
        this._showBackIcon = false;
        this._backCallback = undefined;
    }

    /**
     * set the back callback handler
     *
     * @param handler
     */
    public setBackHandler(handler: () => void) {
        this._backCallback = handler;
    }
}
