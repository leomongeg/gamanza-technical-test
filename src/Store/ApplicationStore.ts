import { Service } from "typedi";
import { create }  from "mobx-persist";
import BaseStore   from "./Base/BaseStore";
import LoadStore   from "./LoadStore/LoadStore";

/**
 * Store Object type interface definition
 */
export interface StoreObjectType<T> {
    new(applicationStore: ApplicationStore): T;

    NAME_STORE: string;
}

@Service()
export class ApplicationStore {
    private stores: { [key: string]: BaseStore } = {};

    /**
     * Subscribe function
     * @param store
     */
    public subscribe(store: StoreObjectType<BaseStore>) {
        this.stores[store.NAME_STORE] = new store(this);
    }

    /**
     * Return the list of available stores
     */
    public getStores = () => this.stores;

    /**
     * Return a store instance
     * @param store
     * @return {T}
     */
    public getStore<T>(store: StoreObjectType<T>): T {
        const storeName = store.NAME_STORE;
        const value     = this.getStores()[storeName];

        if (!value)
            throw `Error: the Store ${storeName} don't exist or not subscribed`;

        return <T><unknown>value;
    }

    constructor() {
        // onError(error => {
        //     console.log(error);
        // });
        LoadStore.map(store => this.subscribe(store));
    }

    /**
     * Init Mobx Storage
     */
    public async initStorage() {
        const hydrate = await create();

        for (const key in this.getStores()) {
            if (this.getStores()[key].getNeedPersistData())
                await hydrate(key, this.getStores()[key], {});
        }
    }
}
