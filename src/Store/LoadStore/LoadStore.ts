import { ConfigurationStore } from "../ConfigurationStore";
import { TokenStore }         from "../TokenStore";
import { UserStore }          from "../UserStore";
import { ErrorBagStore }      from "../ErrorBagStore";
import { LoggerStore }        from "../../Service/LoggerReportingService";
import { UniversityStore }    from "../UniversityStore";
import { CountryStore }       from "../CountryStore";
import { NavbarStore }        from "../NavbarStore";
import { NotificationStore }  from "../NotificationStore";


export default [
    ConfigurationStore,
    ErrorBagStore,
    LoggerStore,
    TokenStore,
    UserStore,
    UniversityStore,
    CountryStore,
    NavbarStore,
    NotificationStore
];
