import { observable } from "mobx";
import { persist }    from "mobx-persist";
import User           from "../Models/User/User";
import BaseStore      from "./Base/BaseStore";
import AjaxService    from "../Service/AjaxService";
import Container      from "typedi";
import {
    date,
    deserialize,
    serializable
}                     from "serializr";
import { TokenStore } from "./TokenStore";

export class UserStore extends BaseStore {
    public static readonly NAME_STORE: string = "UserStore";

    @persist("object", User)
    @observable
    private user: User | undefined;

    @persist("object")
    @serializable(date())
    private previousCallAccess: Date;

    private callMe: boolean = false;

    /**
     * Implementation of the init function
     */
    protected init() {
        this.needPersistData = true;
    }

    /**
     * Ajax Service instance
     * @return {AjaxService}
     */
    public getAjaxService(): AjaxService {
        return Container.get(AjaxService);
    }

    /**
     * Previous access
     */
    public getPreviousAccess(): Date {
        return this.previousCallAccess;
    }

    /**
     * Return the token store instance
     * @return {TokenStore}
     */
    private getTokenStore(): TokenStore {
        return this.applicationStore.getStore(TokenStore);
    }

    /**
     * get user
     */
    public getUser(): User | undefined {
        // const now = moment().add(2, "h");

        // if (!this.getPreviousAccess() || now.isBefore(moment(this.getPreviousAccess()))) {
        //     const token = this.getTokenStore().getAccessToken();
        //     if (!token) return undefined;
        //     const payload: any = JwtDecode(token.accessToken);
        //     this.getUserMeApi(payload.id);
        // }

        return this.user;
    }


    /**
     * setter previous access
     *
     * @param currentDate
     */
    public setPreviousAccess(currentDate: Date) {
        this.previousCallAccess = currentDate;
    }

    /**
     * set user
     *
     * @param user
     */
    public setUser(user: User | undefined): this {
        this.user = user;

        return this;
    }

    /**
     * Bind the authenticated user by access token to the UserStore to identified the current user.
     */
    public async bindSignedInUser(): Promise<boolean> {
        // @TODO: Implement

        return true;
    }
}
