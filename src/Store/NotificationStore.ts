import BaseStore      from "./Base/BaseStore";
import { observable } from "mobx";

export enum NotificationSeverity {
    SUCCESS = "success",
    ERROR   = "error",
    WARNING = "warning",
    INFO    = "info"
}

export class NotificationStore extends BaseStore {
    public static readonly NAME_STORE: string = "NotificationStore";

    @observable
    private _show: boolean = false;

    @observable
    private _message: string = "";

    @observable
    private _severity: NotificationSeverity = NotificationSeverity.INFO;

    /**
     * Implementation of the init function
     */
    protected init() {
        this.needPersistData = false;
    }

    get show(): boolean {
        return this._show;
    }

    get message(): string {
        return this._message;
    }

    get severity(): NotificationSeverity {
        return this._severity;
    }

    /**
     * Show with message
     *
     * @param message
     */
    public showWithMessageAndSeverity(message: string, severity: NotificationSeverity = NotificationSeverity.INFO): void {
        this._message  = message;
        this._severity = severity;
        this._show     = true;
    }

    /**
     * Hide notification
     */
    public hideNotification() {
        this._show    = false;
        this._message = "";
    }

}
