import BaseStore            from "./Base/BaseStore";
import { Container }        from "typedi";
import AxiosService         from "../Service/AxiosService";
import Country              from "../Models/Country/Country";
import { observable }       from "mobx";
import { CountryStore }     from "./CountryStore";
import { ApplicationStore } from "./ApplicationStore";
import API_CONST            from "../config/API_CONST";
import University           from "../Models/University/University";
import { url_for }          from "../Utils/url_generator";
import {
    NotificationSeverity,
    NotificationStore
}                           from "./NotificationStore";

export class UniversityStore extends BaseStore {
    public static readonly NAME_STORE: string = "UniversityStore";

    /**
     * Implementation of the init function
     */
    protected init() {
        this.needPersistData = false;
    }

    @observable
    private _country: Country;

    @observable
    private _universitiesDB: University[] = [];

    @observable
    private _universities: University[] = [];

    @observable
    private _isFetchingData: boolean = false;

    @observable
    private _hasErrors: boolean = false;

    @observable
    private _filterText: string = "";

    private _pageNumber: number = 0;

    private _pageSize: number = 10;

    private _isSortAsc: boolean = true;

    @observable
    private _count: number = 0;


    get country(): Country {
        return this._country;
    }

    get universities(): University[] {
        return this._universities;
    }

    get isFetchingData(): boolean {
        return this._isFetchingData;
    }

    get hasErrors(): boolean {
        return this._hasErrors;
    }

    get filterText(): string {
        return this._filterText;
    }

    get pageNumber(): number {
        return this._pageNumber;
    }

    get pageSize(): number {
        return this._pageSize;
    }

    get count(): number {
        return this._count;
    }

    get isSortAsc(): boolean {
        return this._isSortAsc;
    }

    /**
     * Fetch data from remote API and serialize to store.
     */
    public async fetchData(): Promise<void> {
        if (!this.country) throw Error("Country is required in University Store");
        this._isFetchingData   = true;
        const axios            = Container.get(AxiosService);
        const params           = { country: this.country.name };
        const { status, data } = await axios.axios.get(API_CONST.GET_SEARCH, {
            params
        });

        if (status !== 200) {
            this.applicationStore
                .getStore(NotificationStore)
                .showWithMessageAndSeverity("An error occurs trying to fetch Universities from the remote API",
                                            NotificationSeverity.ERROR);
            this._isFetchingData = false;
            return;
        }
        this._universitiesDB = University.deserializeEntity(data, this.country);

        this.applySortingCriteria();
        this.updateDisplayData();
        this._isFetchingData = false;
    }

    /**
     * Clean the store on exit
     */
    public cleanStore() {
        this._pageNumber     = 0;
        this._universities   = [];
        this._universitiesDB = [];
        this._filterText     = "";
    }

    /**
     * Apply the sort criteria to the main data collection.
     * @private
     */
    private applySortingCriteria() {
        this._universitiesDB = this._universitiesDB.slice().sort((a, b) =>
                                                                     this.isSortAsc ?
                                                                         a.name.localeCompare(b.name) : b.name.localeCompare(a.name));
    }

    /**
     * Updates the store with the data to be displayed and notify to the view to render the changes.
     *
     * @private
     */
    private updateDisplayData() {
        const filtered     = this._universitiesDB
                                 .filter(university => this._filterText !== ""
                                     ? university.name.toLowerCase().includes(this._filterText.toLowerCase()) : true);
        this._universities = filtered.slice(this._pageNumber * this._pageSize, (this._pageNumber + 1) * this._pageSize);

        this._count = filtered.length;
    }

    /**
     * Define the new page number
     * @param pageNumber
     */
    public updatePage(pageNumber: number) {
        this._pageNumber = pageNumber;
        this.updateDisplayData();
    }

    /**
     * Update the page size
     * @param size
     */
    public updatePageSize(size: number) {
        this._pageSize   = size;
        this._pageNumber = 0;
        this.updateDisplayData();
    }

    /**
     * Change the sort criteria.
     */
    public changeSort() {
        this._isSortAsc = !this._isSortAsc;
        this.applySortingCriteria();
        this.updateDisplayData();
    }

    /**
     * Set the filter text and trigger the search.
     * @param text
     */
    public setFilterText(text: string) {
        this._filterText = text;
    }

    /**
     * Trigger the filters over the data.
     */
    public applyFilters() {
        this._pageNumber = 0;
        this.updateDisplayData();
    }

    /**
     * Load the universities if the code is invalid redirect to 404 page
     * @param code
     */
    public async fetchUniversitiesByCountryCode(code: string, history: any): Promise<void> {
        const countryStore = Container.get(ApplicationStore).getStore(CountryStore);
        const country      = await countryStore.findByCode(code);

        if (!country) {
            history.push(url_for("not-found"));
            return;
        }

        this._country = country;
        return this.fetchData();
    }
}
