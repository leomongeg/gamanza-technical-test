import BaseStore      from "./Base/BaseStore";
import { observable } from "mobx";

export class ErrorBagStore extends BaseStore {
    public static readonly ErrorBagStore: string = "ErrorBagStore";

    @observable
    private _errors: Map<string, { [key: string]: any }>;

    /**
     * Init function
     */
    protected init() {
        this.needPersistData = false;
        if (!this._errors) this._errors = new Map<string, any[]>();
        if (!this._errors.has("default")) {
            this._errors.set("default", []);
        }
    }

    /**
     * Initialize the error bag for an custom alias, the default is default
     * @param alias
     */
    public initialize(alias?: string): void {
        if (alias && alias !== "default") {
            this._errors.set(alias, []);
        }
    }


    /**
     * Clean the bag for alias, default: default
     * @param alias
     */
    public cleanBagFor(alias?: string): void {
        if (this._errors.has(alias || "default")) {
            this._errors.set(alias || "default", []);
        }
    }

    /**
     * Add items to the bag
     * @param info
     * @param alias
     */
    public fillBag(key: string, info: any, alias?: string): this {
        const bag = this._errors.get(alias || "default");
        if (bag) bag[key] = info;

        return this;
    }

    /**
     * Return the content for a bag
     * @param alias
     */
    public getBag(alias?: string): any | undefined {
        return !this._errors.has(alias || "default") ? undefined : this._errors.get(alias || "default");
    }

    get errors(): Map<string, { [p: string]: any }> {
        return this._errors;
    }
}