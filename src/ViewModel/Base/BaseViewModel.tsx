import { Component }                         from "react";
import { Container }                         from "typedi";
import { ApplicationStore, StoreObjectType } from "../../Store/ApplicationStore";
import { ObjectType }                        from "typedi/types/ObjectType";

export class BaseViewModel<P, S> extends Component<P, S> {
    /**
     * Get a Store instance.
     *
     * @param storeType
     */
    protected getStore<T>(store: StoreObjectType<T>) {
        return Container.get(ApplicationStore).getStore(store);
    }

    /**
     * Get a Service instance.
     *
     * @param type
     * @protected
     */
    protected getService<T>(type: ObjectType<T>): T {
        return Container.get(type);
    }
}