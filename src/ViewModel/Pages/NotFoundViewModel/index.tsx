import React, { useEffect } from "react";
import {
    Paper,
    makeStyles
}                           from "@material-ui/core";
import WarningIcon          from "@material-ui/icons/Warning";
import { NavbarStore }      from "../../../Store/NavbarStore";
import { Container }        from "typedi";
import { ApplicationStore } from "../../../Store/ApplicationStore";
import { useHistory }       from "react-router-dom";
import { url_for }          from "../../../Utils/url_generator";

const useStyles = makeStyles(theme => ({
    paper: {
        width       : "100%",
        marginBottom: theme.spacing(2),
        textAlign   : "center"
    }
}));

const NotFoundViewModel = () => {
    const classes = useStyles();
    const history = useHistory();

    /**
     * Component did mount
     */
    useEffect(() => {
        const navbarStore: NavbarStore = Container.get(ApplicationStore).getStore(NavbarStore);
        navbarStore.enableBackButton();
        navbarStore.setBackHandler(() => {
            history.push(url_for("home-page"));
        });
        return () => {
            // Component did unmount
            navbarStore.disableBackButton();
        };
    }, []);

    return <Paper className={ classes.paper }><WarningIcon/> Resource Not found</Paper>;
};

export default NotFoundViewModel;
