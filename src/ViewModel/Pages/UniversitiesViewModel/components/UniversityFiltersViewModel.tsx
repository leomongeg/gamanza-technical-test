import React, {
    useCallback,
    useEffect
}                           from "react";
import {
    Button,
    Grid,
    InputAdornment,
    makeStyles,
    Paper,
    TextField,

}                           from "@material-ui/core";
import SearchIcon           from "@material-ui/icons/Search";
import { UniversityStore }  from "../../../../Store/UniversityStore";
import { Container }        from "typedi";
import { useObserver }      from "mobx-react";
import {
    WithTranslation,
    withTranslation
}                           from "react-i18next";
import { ApplicationStore } from "../../../../Store/ApplicationStore";

const useStyles = makeStyles(theme => ({
    root           : {
        flexGrow: 1,
    },
    fullWidth      : {
        marginBottom: "15px",
        width       : "100%",
        margin      : theme.spacing(1),
    },
    paper          : {
        padding  : theme.spacing(2),
        textAlign: "center",
        color    : theme.palette.text.secondary,
    },
    buttonContainer: {
        textAlign: "left",
        "& > *"  : {
            margin: theme.spacing(1),
        }
    }
}));

/**
 * Props definition interface
 */
interface UniversityFiltersViewModelProps extends WithTranslation {
}

const UniversityFiltersViewModel = ({ t }: UniversityFiltersViewModelProps) => {
    const universityStore: UniversityStore = Container.get(ApplicationStore).getStore(UniversityStore);
    const classes                          = useStyles();
    let searchDelay: any;

    /**
     * Memoized callback to avoid re-renders on every user key up.
     */
    const updateSearchCriteria = useCallback((text: string) => {
        universityStore.setFilterText(text);

        // Avoid try to search for every key pressed only when the user stop typing.
        clearTimeout(searchDelay);
        searchDelay = setTimeout(() => {
            universityStore.applyFilters();
        }, 500);
    }, [ searchDelay ]);

    /**
     * Component did mount
     */
    useEffect(() => {
        return () => {
            // Component did unmount
            clearTimeout(searchDelay);
        };
    }, []);

    return useObserver(() => (<Grid item>
        <Paper className={ classes.paper }>

            <TextField className={ classes.fullWidth }
                       label={ t("universityPage:university") }
                       disabled={ universityStore.isFetchingData }
                       InputProps={ {
                           startAdornment: (
                               <InputAdornment position="start">
                                   <SearchIcon/>
                               </InputAdornment>
                           ),
                       } }
                       onChange={ (evt) => {
                           updateSearchCriteria(evt.target.value);
                       } }
                       value={ universityStore.filterText }
            />
            <Grid item xs={ 12 } sm={ 12 } className={ classes.buttonContainer }>
                <Button
                    onClick={ () => {
                        updateSearchCriteria("");
                    } }
                    variant="outlined">
                    { t("__clean__") }</Button>
            </Grid>
        </Paper>
    </Grid>));
};

export default withTranslation("")(UniversityFiltersViewModel);
