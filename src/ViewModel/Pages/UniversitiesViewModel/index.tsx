import React, {
    useEffect,
    useState
}                                 from "react";
import {
    useObserver
}                                 from "mobx-react";
import {
    withTranslation
}                                 from "react-i18next";
import IBaseProps                 from "../../Props/IBaseProps";
import { UniversityStore }        from "../../../Store/UniversityStore";
import { Container }              from "typedi";
import TableToolbarView           from "../../Components/Table/TableToolbarView";
import {
    makeStyles,
    CircularProgress,
    Link,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow,
    TableSortLabel
}                                 from "@material-ui/core";
import {
    useHistory,
    useRouteMatch
}                                 from "react-router-dom";
import UniversityFiltersViewModel from "./components/UniversityFiltersViewModel";
import { NavbarStore }            from "../../../Store/NavbarStore";
import { ApplicationStore }       from "../../../Store/ApplicationStore";
import { url_for }                from "../../../Utils/url_generator";

const useStyles = makeStyles(theme => ({
    root          : {
        width: "100%",
    },
    paper         : {
        width       : "100%",
        marginBottom: theme.spacing(2),
    },
    table         : {
        minWidth: 750,
    },
    visuallyHidden: {
        border  : 0,
        clip    : "rect(0 0 0 0)",
        height  : 1,
        margin  : -1,
        overflow: "hidden",
        padding : 0,
        position: "absolute",
        top     : 20,
        width   : 1,
    },
    loader        : {
        textAlign  : "right",
        marginRight: "2.5em",
        paddingTop : "0.8em",
        minHeight  : "52px"
    }
}));


/**
 * Route params
 */
interface UniversitiesViewModelRouteParams {
    code: string;
}

/**
 * Props definition interface
 */
interface UniversitiesViewModelProps extends IBaseProps {
}

const UniversitiesViewModel = ({ t }: UniversitiesViewModelProps) => {
    const universityStore: UniversityStore = Container.get(ApplicationStore).getStore(UniversityStore);
    const navbarStore: NavbarStore         = Container.get(ApplicationStore).getStore(NavbarStore);
    const classes                          = useStyles();
    const history                          = useHistory();
    const match                            = useRouteMatch();
    const [ showFilters, toggleFilters ]   = useState(false);
    const tableSchema: any[]               = [ {
        columnName   : t("nameColumn"),
        sortable     : true,
        sortDirection: universityStore.isSortAsc,
        sortHandler  : () => universityStore.changeSort()
    }, {
        columnName: t("websiteColumn"),
        sortable  : false
    }, {
        columnName: t("stateColumn"),
        sortable  : false
    }, {
        columnName: t("domainColumn"),
        sortable  : false
    } ];

    /**
     * Component did mount
     */
    useEffect(() => {
        const { code } = (match.params as UniversitiesViewModelRouteParams);

        universityStore.fetchUniversitiesByCountryCode(code, history);
        navbarStore.enableBackButton();
        navbarStore.setBackHandler(() => {
            if (history.action === "POP") {
                history.push(url_for("home-page"));
                return;
            }
            history.goBack();
        });
        return () => {
            // Component did unmount
            universityStore.cleanStore();
            navbarStore.disableBackButton();
        };
    }, []);

    return useObserver(() => (<Paper className={ classes.paper }>
        <TableToolbarView title={ t("tableTitle", { country: universityStore.country?.name || "..." }) }
                          isLoading={ universityStore.isFetchingData }
                          addNew={ false }
                          toggleFilter={ () => toggleFilters(!showFilters) }
                          hasFilters={ true }/>
        { showFilters && <UniversityFiltersViewModel/> }
        <TableContainer>
            <Table
                className={ classes.table }
            >
                <TableHead>
                    <TableRow>
                        {
                            tableSchema.map(({ columnName, sortable, sortDirection, sortHandler }) => (<TableCell
                                key={ columnName.toLocaleLowerCase().replace(" ", "-") }
                                align={ "left" }
                                padding={ "default" }

                            >
                                <TableSortLabel
                                    active={ sortable }
                                    direction={ !!sortDirection ? "asc" : "desc" }
                                    onClick={ sortHandler }
                                >
                                    { t(columnName) }
                                </TableSortLabel>
                            </TableCell>))
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        universityStore.universities.length > 0 && universityStore.universities.map(({
                                                                                                         id,
                                                                                                         name,
                                                                                                         webPage,
                                                                                                         state,
                                                                                                         domains
                                                                                                     }, index) => (
                            <TableRow key={ id }>
                                <TableCell>{ name }</TableCell>
                                <TableCell style={ { width: 200 } }>
                                    { webPage.length > 0 ?
                                        <Link href={ webPage[0] } target="_blank">{ webPage[0] }</Link> : "N/A" }
                                </TableCell>
                                <TableCell style={ { width: 200 } }>
                                    { `${ state ? state : "N/A" }` }
                                </TableCell>
                                <TableCell style={ { width: 200 } }>
                                    { domains.length > 0 ? domains[0] : "N/A" }
                                </TableCell>
                            </TableRow>)
                        )
                    }
                </TableBody>
            </Table>
            {
                universityStore.isFetchingData
                    ? <div className={ classes.loader }><CircularProgress size={ 24 } thickness={ 5 }/></div>
                    : <TablePagination
                        rowsPerPageOptions={ [ 5, 10, 25, 50, 100 ] }
                        component="div"
                        count={ universityStore.count }
                        rowsPerPage={ universityStore.pageSize }
                        page={ universityStore.pageNumber }
                        onChangePage={ (evt: unknown, newPage: number) => universityStore.updatePage(newPage) }
                        onChangeRowsPerPage={ (event: React.ChangeEvent<HTMLInputElement>) =>
                            universityStore.updatePageSize(parseInt(event.target.value, 10)) }
                    />
            }
        </TableContainer>
    </Paper>));
};

export default withTranslation("universityPage")(UniversitiesViewModel);
