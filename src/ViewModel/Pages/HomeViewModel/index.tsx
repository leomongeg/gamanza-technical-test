import * as React        from "react";
import { observer }      from "mobx-react";
import {
    withTranslation
}                        from "react-i18next";
import { withRouter }    from "react-router";
import { BaseViewModel } from "../../Base/BaseViewModel";
import IBaseProps        from "../../Props/IBaseProps";
import View              from "./view";
import { CountryStore }  from "../../../Store/CountryStore";
import { observable }    from "mobx";
import { url_for }       from "../../../Utils/url_generator";


/**
 * Props definition interface
 */
interface HomeViewModelProps extends IBaseProps {
}

@observer
class HomeViewModel extends BaseViewModel<HomeViewModelProps, any> {
    private countryStore: CountryStore;

    @observable
    private showFilters: boolean = false;

    constructor(props: HomeViewModelProps) {
        super(props);
        this.countryStore = this.getStore(CountryStore);
    }

    /**
     * Life cycle event
     */
    componentDidMount() {
        this.countryStore.fetchData();
        this.showFilters = (this.countryStore.filterText !== "");
    }

    private toggleFilterHandler = () => {
        this.showFilters = !this.showFilters;
        if (!this.showFilters) this.countryStore.clearFilters();
    };

    private viewUniversities = (code: string) => {
        const { history } = this.props;
        history.push(url_for("universities", { code: code.toLocaleLowerCase() }));
    };

    /**
     * Component render
     */
    public render(): React.ReactNode {
        const { t } = this.props;
        return (
            <View
                isLoading={ this.countryStore.isFetchingData }
                toggleFilter={ () => this.toggleFilterHandler() }
                showFilters={ this.showFilters }
                items={ this.countryStore.countries }
                tableSchema={ [ {
                    columnName   : t("countryColumn"),
                    sortable     : true,
                    sortDirection: this.countryStore.isSortAsc,
                    sortHandler  : () => this.countryStore.changeSort()
                }, {
                    columnName: t("flagColumn"),
                    sortable  : false
                } ] }
                count={ this.countryStore.count }
                rowsPerPage={ this.countryStore.pageSize }
                page={ this.countryStore.pageNumber }
                onChangePage={ (pageNumber: number) => this.countryStore.updatePage(pageNumber) }
                handleRowsPerPageChange={ (size: number) => this.countryStore.updatePageSize(size) }
                viewUniversities={ this.viewUniversities }
            />
        );
    }
}

// @ts-ignore Ignore issue with typings in withRouter
export default withRouter(withTranslation("homePage")(HomeViewModel));
