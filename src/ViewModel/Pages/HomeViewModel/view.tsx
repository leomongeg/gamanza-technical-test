import React, { Component }    from "react";
import Country                 from "../../../Models/Country/Country";
import TableToolbarView        from "../../Components/Table/TableToolbarView";
import {
    createStyles,
    WithStyles,
    withStyles,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TableSortLabel,
    TablePagination,
    CircularProgress,
    Link
}                              from "@material-ui/core";
import {
    WithTranslation,
    withTranslation
}                              from "react-i18next";
import CountryFiltersViewModel from "./components/CountryFiltersViewModel";
import { observer }            from "mobx-react";

const ReactCountryFlag = require("react-country-flag").default;

/**
 * Home View Props
 */
interface HomeViewProps extends WithTranslation, WithStyles {
    isLoading: boolean;
    toggleFilter: () => void;
    showFilters: boolean;
    items: Country[];
    tableSchema: { columnName: string, sortable: boolean, sortDirection?: boolean, sortHandler?: () => void }[];
    count: number;
    rowsPerPage: number;
    page: number;
    onChangePage: (newPage: number) => void;
    handleRowsPerPageChange: (itemsPerPage: number) => void;
    viewUniversities: (code: string) => void;
}

@observer
class View extends Component <HomeViewProps, any> {
    /**
     * Render Function
     */
    render() {
        const {
                  t, classes, isLoading, toggleFilter, items, tableSchema, count, rowsPerPage, page, onChangePage,
                  handleRowsPerPageChange, showFilters, viewUniversities
              } = this.props;

        return (<Paper className={ classes.paper }>
            <TableToolbarView title={ t("tableTitle") }
                              isLoading={ isLoading }
                              addNew={ false }
                              toggleFilter={ toggleFilter }
                              hasFilters={ true }/>
            { showFilters && <CountryFiltersViewModel/> }
            <TableContainer>
                <Table
                    className={ classes.table }
                >
                    <TableHead>
                        <TableRow>
                            {
                                tableSchema.map(({ columnName, sortable, sortDirection, sortHandler }) => (<TableCell
                                    key={ columnName.toLocaleLowerCase().replace(" ", "-") }
                                    align={ "left" }
                                    padding={ "default" }

                                >
                                    <TableSortLabel
                                        active={ sortable }
                                        direction={ !!sortDirection ? "asc" : "desc" }
                                        onClick={ sortHandler }
                                    >
                                        { t(columnName) }
                                    </TableSortLabel>
                                </TableCell>))
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            items.length > 0 ? items.map(({
                                                              name,
                                                              code
                                                          }) => (
                                <TableRow key={ code }>
                                    <TableCell>
                                        <Link href="#"
                                              onClick={ (evt: React.SyntheticEvent) => {
                                                  evt.preventDefault();
                                                  viewUniversities(code);
                                              } }
                                        >{ name }</Link></TableCell>
                                    <TableCell style={ { width: 200 } }>
                                        <ReactCountryFlag
                                            countryCode={ code }
                                            svg
                                            style={ {
                                                fontSize: "4em"
                                            } }
                                            aria-label={ name }/>
                                    </TableCell>
                                </TableRow>)
                            ) : undefined
                        }
                    </TableBody>
                </Table>
            </TableContainer>
            {
                isLoading ? <div className={ classes.loader }><CircularProgress size={ 24 } thickness={ 5 }/></div>
                    : <TablePagination
                        rowsPerPageOptions={ [ 5, 10, 25, 50, 100 ] }
                        component="div"
                        count={ count }
                        rowsPerPage={ rowsPerPage }
                        page={ page }
                        onChangePage={ (evt: unknown, newPage: number) => { onChangePage(newPage); } }
                        onChangeRowsPerPage={ (event: React.ChangeEvent<HTMLInputElement>) => {
                            handleRowsPerPageChange(parseInt(event.target.value, 10));
                        } }
                    />
            }
        </Paper>);
    }
}

const styles = createStyles((theme: any) => ({
    root          : {
        width: "100%",
    },
    paper         : {
        width       : "100%",
        marginBottom: theme.spacing(2),
    },
    table         : {
        minWidth: 750,
    },
    visuallyHidden: {
        border  : 0,
        clip    : "rect(0 0 0 0)",
        height  : 1,
        margin  : -1,
        overflow: "hidden",
        padding : 0,
        position: "absolute",
        top     : 20,
        width   : 1,
    },
    loader        : {
        textAlign  : "right",
        marginRight: "2.5em",
        paddingTop : "0.8em",
        minHeight  : "52px"
    }
}));

export default withStyles(styles)(withTranslation("homePage")(View));
