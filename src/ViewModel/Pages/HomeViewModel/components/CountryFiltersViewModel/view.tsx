import React, { Component } from "react";

import {
    createStyles,
    WithStyles,
    withStyles,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TableSortLabel,
    FormControl,
    InputLabel,
    InputAdornment,
    // AccountCircle,
    TextField,
    Button,
    Grid,
    CircularProgress
}                           from "@material-ui/core";
import SearchIcon           from "@material-ui/icons/Search";
import SimpleReactValidator from "simple-react-validator";
import { withTranslation }  from "react-i18next";
import IBaseProps           from "../../../../Props/IBaseProps";
import { observer }         from "mobx-react";

/**
 * Component props
 */
interface CountryFiltersViewProps extends IBaseProps, WithStyles {
    ajaxWorking: boolean;
    handleClean: () => void;
    filterText: string;
    setFilterText: (val: string) => void;
}

@observer
class View extends Component<CountryFiltersViewProps, any> {
    /**
     * View Render
     */
    render() {
        const { t, classes, ajaxWorking, handleClean, filterText, setFilterText } = this.props;
        return (
            <Grid item>
                <Paper className={ classes.paper }>

                    <TextField className={ classes.fullWidth }
                               label="Country"
                               disabled={ajaxWorking}
                               InputProps={ {
                                   startAdornment: (
                                       <InputAdornment position="start">
                                           <SearchIcon/>
                                       </InputAdornment>
                                   ),
                               } }
                               onChange={(evt) => {
                                   setFilterText(evt.target.value);
                               }}
                               value={filterText}
                    />
                    <Grid item xs={ 12 } sm={ 12 } className={ classes.buttonContainer }>
                        <Button
                            onClick={ () => { handleClean(); } }
                            variant="outlined">
                            { t("__clean__") }</Button>
                    </Grid>
                </Paper>
            </Grid>
        );
    }
}

const styles = createStyles((theme: any) => ({
    root           : {
        flexGrow: 1,
    },
    fullWidth      : {
        marginBottom: "15px",
        width       : "100%",
        margin: theme.spacing(1),
    },
    paper          : {
        padding  : theme.spacing(2),
        textAlign: "center",
        color    : theme.palette.text.secondary,
    },
    buttonContainer: {
        textAlign: "left",
        "& > *"  : {
            margin: theme.spacing(1),
        }
    }
}));
export default withStyles(styles)(withTranslation("")(View));
