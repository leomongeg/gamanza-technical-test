import React             from "react";
import { BaseViewModel } from "../../../../Base/BaseViewModel";
import View              from "./view";
import { CountryStore }  from "../../../../../Store/CountryStore";
import { observer }      from "mobx-react";

/**
 * Props definition interface
 */
interface CountryFiltersViewModelProps {
}

@observer
class CountryFiltersViewModel extends BaseViewModel<CountryFiltersViewModelProps, any> {
    private countryStore: CountryStore;

    private searchDelay: any;

    constructor(props: CountryFiltersViewModelProps) {
        super(props);
        this.countryStore = this.getStore(CountryStore);
    }

    private updateSearchCriteria = (text: string) => {
        this.countryStore.setFilterText(text);

        // Avoid try to search for every key pressed only when the user stop typing.
        clearTimeout(this.searchDelay);
        this.searchDelay = setTimeout(() => {
            this.countryStore.applyFilters();
        }, 500);
    };

    /**
     * Life cycle event
     */
    componentWillUnmount() {
        clearTimeout(this.searchDelay);
    }

    /**
     * Render view model view.
     */
    render() {
        return <View
            ajaxWorking={ this.countryStore.isFetchingData }
            handleClean={ () => this.updateSearchCriteria("") }
            filterText={ this.countryStore.filterText }
            setFilterText={ this.updateSearchCriteria }
        />;
    }
}

export default CountryFiltersViewModel;
