import React                    from "react";
import { withTranslation }      from "react-i18next";
import {
    Snackbar
}                               from "@material-ui/core";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";
import { useObserver }          from "mobx-react";
import { Container }            from "typedi";
import { ApplicationStore }     from "../../../Store/ApplicationStore";
import { NotificationStore }    from "../../../Store/NotificationStore";


const Alert = (props: AlertProps) => {
    return <MuiAlert elevation={ 6 } variant="filled" { ...props } />;
};

const NotificationViewModel = () => {
    const notificationStore = Container.get(ApplicationStore).getStore(NotificationStore);

    return useObserver(() => (<div>
        <Snackbar open={ notificationStore.show } autoHideDuration={ 60000 }
                  onClose={ () => notificationStore.hideNotification() }
                  ClickAwayListenerProps={ { onClickAway: (event) => {event.preventDefault(); } } }
        >
            <Alert onClose={ () => notificationStore.hideNotification() } severity={ notificationStore.severity }>
                { notificationStore.message }
            </Alert>
        </Snackbar>
    </div>));
};

export default withTranslation("")(NotificationViewModel);
