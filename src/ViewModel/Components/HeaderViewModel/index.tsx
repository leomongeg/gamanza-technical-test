import * as React          from "react";
import { BaseViewModel }   from "../../Base/BaseViewModel";
import { observer }        from "mobx-react";
import { NavbarStore }     from "../../../Store/NavbarStore";
import {
    withRouter,
    RouteComponentProps
}                          from "react-router";
import { url_for }         from "../../../Utils/url_generator";
import View                from "./view";
import {
    WithTranslation,
    withTranslation
} from "react-i18next";

/**
 * Component props
 */
interface HeaderViewModelProps extends RouteComponentProps, WithTranslation {

}

@observer
class HeaderViewModel extends BaseViewModel<HeaderViewModelProps, any> {
    private navBarStore: NavbarStore;

    constructor(props: HeaderViewModelProps) {
        super(props);
        this.navBarStore       = this.getStore(NavbarStore);
        const { t }            = this.props;
        this.navBarStore.title = t("pageTitle");
    }

    /**
     * render
     */
    public render(): React.ReactNode {
        return (
            <View
                title={ this.navBarStore.title }
                showBack={ this.navBarStore.showBackIcon }
                handleBack={ () => {
                    if (this.navBarStore.backCallback) {
                        this.navBarStore.backCallback();
                        return;
                    }
                    const { history } = this.props;
                    history.push(url_for("home-page"));
                } }
            />
        );
    }
}

// @ts-ignore
export default withRouter(withTranslation("homePage")(HeaderViewModel));
