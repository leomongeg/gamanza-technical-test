import React, {
    Component,
    ReactNode
}                          from "react";
import { withTranslation } from "react-i18next";
import IBaseProps          from "../../../ViewModel/Props/IBaseProps";
import AppBar              from "@material-ui/core/AppBar";
import IconButton          from "@material-ui/core/IconButton";
import Toolbar             from "@material-ui/core/Toolbar";
import ArrowBackIcon       from "@material-ui/icons/ArrowBack";
import Typography          from "@material-ui/core/Typography";
import {
    createStyles,
    withStyles,
    withTheme
}                          from "@material-ui/core";
import { observer }        from "mobx-react";

/**
 * Props interface
 */
interface HeaderViewProps extends IBaseProps {
    title: string;
    showBack: boolean;
    handleBack: () => void;
}

@observer
class View extends Component<HeaderViewProps, any> {
    /**
     * render
     */
    public render(): ReactNode {
        const { t, classes, title, showBack, handleBack } = this.props;

        return (<AppBar position="fixed" className={ classes.appBar }>
            <Toolbar>
                { showBack ? <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                    onClick={ handleBack }
                    className={ classes.menuButton }
                >
                    <ArrowBackIcon/>
                </IconButton> : undefined }
                <Typography variant="h6" noWrap>
                    { t(title) }
                </Typography>
            </Toolbar>
        </AppBar>);
    }
}

const styles = createStyles((theme: any) => ({
    appBar    : {},
    menuButton: {
        marginRight: theme.spacing(2)
    },
    toolbar   : theme.mixins.toolbar,
}));

export default withStyles(styles)(withTheme(withTranslation("common")(View)));
