const variables: any = require("../../Styles/common/variables.scss");

const scssConfiguration = {
    prefix: variables.prefix,
    primaryColor: variables.primaryColor,
    quaternaryColor: variables.quaternaryColor,
    secondaryColor: variables.secondaryColor,
    tertiaryColor: variables.tertiaryColor
};

export default scssConfiguration;
