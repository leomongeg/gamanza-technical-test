/**
 * Default Language
 */

const es = {
    header        : {
        title: "Universities by Country"
    },
    homePage      : {
        tableTitle   : "Countries",
        countryColumn: "Country",
        flagColumn   : "Flag",
    },
    universityPage: {
        tableTitle   : "Universities {{country}}",
        university   : "University",
        nameColumn   : "University name",
        websiteColumn: "Website",
        stateColumn  : "State - Province",
        domainColumn : "Domain"
    },
    common        : {
        __clean__          : "Clean search",
        filter_list_tooltip: "Filters",
        pageTitle          : "Gamanza Technical Test"
    }
};

export default es;
