import AccessToken                        from "../Models/Security/AccessToken";
import User                               from "../Models/User/User";
import { Container, Service }             from "typedi";
import AxiosService                       from "./AxiosService";
import { UserStore }                      from "../Store/UserStore";
import { TokenStore }                     from "../Store/TokenStore";
import { ApplicationStore }               from "../Store/ApplicationStore";
import { generatePath }                   from "react-router";
import AjaxService                        from "./AjaxService";
import { url_for }                        from "../Utils/url_generator";

@Service()
export default class AuthService {
    /**
     * Return the axios service instance
     * @return {AxiosService}
     */
    private getAxiosService(): AxiosService {
        return Container.get(AxiosService);
    }

    /**
     * Return ajax service instance
     * @return {AjaxService}
     */
    private getAjaxService(): AjaxService {
        return Container.get(AjaxService);
    }

    /**
     * Return application store instance
     * @return {ApplicationStore}
     */
    private getApplicationStore(): ApplicationStore {
        return Container.get(ApplicationStore);
    }

    /**
     * Return the user store instance
     * @return {UserStore}
     */
    private getUserStore(): UserStore {
        return this.getApplicationStore().getStore(UserStore);
    }

    /**
     * Return the token store instance
     * @return {TokenStore}
     */
    private getTokenStore(): TokenStore {
        return this.getApplicationStore().getStore(TokenStore);
    }

    /**
     * Execute the process to obtain the access token
     * @param tokenSaml
     * @param cb
     * @return {void}
     */
    public async getAccessToken(identification: string, password: string, cb?: (err: boolean, user: User | undefined) => void): Promise<boolean> {
        let accessToken;
        try {
            accessToken = await this.getAxiosService().getAccessToken(identification, password);
        } catch (e) {
            accessToken = undefined;
        }

        // Access Token Invalid
        if (!accessToken) {
            return false;
        }

        const token = new AccessToken(accessToken);

        if (this.getTokenStore())
            this.getTokenStore().setAccessToken(token);

        await this.getUserStore().bindSignedInUser();
        const user = this.getUserStore().getUser();

        if (!this.getUserStore() || !user || user.userId === undefined) {
            if (cb)
                cb(true, undefined);

            return false;
        }

        if (cb)
            cb(false, user);

        return true;
    }

    /**
     * Redirect user to login page
     */
    public redirectToLogin(): void {
        window.location.href = "/login";
    }

    /**
     * Perform logout process and remove all user data.
     */
    public async logout() {
        try {
            await this.getTokenStore().logout();
        } catch (e) {
            // @TODO: Fix the issue with the logout to the BE
        }

        window.location.href = generatePath(url_for("home-page"));
    }

    /**
     * Check if user is authenticated
     */
    public isAuthenticate(): boolean {
        return !!this.getUserStore().getUser();
    }

    /**
     * Return the current logged in user object
     */
    public getUser(): User | undefined {
        return this.getUserStore().getUser();
    }
}
