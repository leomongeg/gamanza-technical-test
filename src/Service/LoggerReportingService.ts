import { Container, Service }             from "typedi";
import { ApplicationStore }               from "../Store/ApplicationStore";
import { UserStore }                      from "../Store/UserStore";
import { detect }                         from "detect-browser";
import Axios, { AxiosInstance }           from "axios";
import { alias, serializable, serialize } from "serializr";
import BaseStore                          from "../Store/Base/BaseStore";
import { persist }                        from "mobx-persist";
import { observable }                     from "mobx";
import LoggerInformationPropSchema        from "../Utils/CustomSerializers/LoggerInformationPropSchema";


export class Log {
    @serializable(alias("exception"))
    private _exception: string;

    @serializable(alias("label", true))
    private _label: string;

    @serializable(alias("serviceName", true))
    private _serviceName: string;

    @serializable(alias("information", LoggerInformationPropSchema))
    private _information: { [key: string]: string }[];

    @serializable(alias("source", true))
    private _source: "frontend";

    @serializable(alias("stackTrace", true))
    private _stackTrace: string;

    @serializable(alias("user", true))
    private _user: string;

    private _type: string;

    constructor() {
        this.type        = "info";
        this.information = [];
        this.source      = "frontend";
    }


    get exception(): string {
        return this._exception;
    }

    set exception(value: string) {
        this._exception = value;
    }

    get label(): string {
        return this._label;
    }

    set label(value: string) {
        this._label = value;
    }

    get serviceName(): string {
        return this._serviceName;
    }

    set serviceName(value: string) {
        this._serviceName = value;
    }

    get information(): { [p: string]: string }[] {
        return this._information;
    }

    set information(value: { [p: string]: string }[]) {
        this._information = value;
    }

    get source(): "frontend" {
        return this._source;
    }

    set source(value: "frontend") {
        this._source = value;
    }

    get stackTrace(): string {
        return this._stackTrace;
    }

    set stackTrace(value: string) {
        this._stackTrace = value;
    }

    get user(): string {
        return this._user;
    }

    set user(value: string) {
        this._user = value;
    }

    get type(): string {
        return this._type;
    }

    set type(value: string) {
        this._type = value;
    }
}

export class LoggerStore extends BaseStore {
    public static readonly NAME_STORE: string = "LoggerStore";

    @persist("list", Log)
    @observable
    private _logsBag: Log[] = [];

    /**
     * Init function
     */
    protected init() {
        this.needPersistData = true;
    }

    get logsBag(): Log[] {
        return this._logsBag;
    }

    /**
     * Clean the logs bag
     */
    public emptyBag(): void {
        this._logsBag = [];
    }
}

type LogScope = { service: LoggerReportingService, log: Log };

@Service()
export default class LoggerReportingService {
    private readonly serviceName: string     = "";
    private readonly axios: AxiosInstance;
    private loggerStore: LoggerStore;
    private autoDispatchInitialized: boolean = false;

    constructor() {
        this.serviceName = process.env.REACT_APP_LOGGER_SERVICE_NAME || "N/A";
        this.axios       = Axios.create({
                                            baseURL: process.env.REACT_APP_LOGGER_ERROR_REPORTING_URL || "",
                                            headers: {
                                                "content-type": "application/json"
                                            }
                                        });
        this.loggerStore = Container.get(ApplicationStore).getStore(LoggerStore);
    }

    /**
     * Create the basic log object
     */
    private createBasicLogObject(): Log {
        const log       = new Log();
        const userStore = Container.get(ApplicationStore).getStore(UserStore);
        log.type        = "info";
        if (userStore.getUser())
            log.user = userStore.getUser()?.getId() || "";

        log.information.push({
                                 bundleVersion: process.env.REACT_APP_VERSION || "N/A",
                                 url          : window.top.location.href.toString()
                             });
        log.information.push(detect() as any);
        log.serviceName = this.serviceName;

        return log;
    }

    /**
     * Create log entry
     *
     * @param type
     */
    info(label?: string, info?: { [p: string]: string }[]): LogScope {
        const log = this.createBasicLogObject();
        log.label = `[INFO] ${label || ""}`;
        info?.forEach((item) => {
            log.information.push(item);
        });

        this.loggerStore.logsBag.push(log);
        return {service: this, log: log};
    }

    /**
     * Create an exception entry log.
     *
     * @param exception
     * @param traceStack
     * @param label
     * @param info
     */
    error(exception: string, traceStack: string, label?: string, info?: { [p: string]: string }[]): LogScope {
        const log = this.createBasicLogObject();
        info?.forEach((item) => {
            log.information.push(item);
        });
        log.type       = "error";
        log.label      = `[ERROR] ${label || ""}`;
        log.exception  = exception;
        log.stackTrace = traceStack;

        this.loggerStore.logsBag.push(log);
        return {service: this, log: log};
    }

    /**
     * Dispatch the current log bag to the server log.
     */
    dispatchLogs(): void {
        this.loggerStore.logsBag.forEach((log: Log) => {
            const endpoint   = log.type === "error" ?
                               process.env.REACT_APP_LOGGER_SERVICE_ERROR_ENDPOINT || "" :
                               process.env.REACT_APP_LOGGER_SERVICE_INFO_ENDPOINT || "";
            const data       = serialize(log);
            data.information = log.information;
            if (process.env.REACT_APP_ENV === "local") {
                console.log("###########################################");
                console.log("       Uncaught Exception Detected         ");
                console.log("###########################################");
                console.error(data);
                console.log("###########################################");
                console.log("");
                console.log("");
                return;
            }

            // new Promise(async (resolve, error) => {
            //     try {
            //         await this.axios.post(endpoint, data);
            //     } catch (e) {
            //         error(e);
            //     }
            //     resolve();
            // }).then((resolve) => {
            // }).catch((error) => {
            //     console.error(error);
            // });
        });
        this.loggerStore.emptyBag();
    }

    /**
     * Start dispatching logs periodically
     * See the env var REACT_APP_LOGGER_DISPATCH_LOGS_INTERVAL_MIN to define the interval.
     */
    setupAutoDispatchLogs() {
        if (this.autoDispatchInitialized) return;
        this.autoDispatchInitialized = true;
        this.dispatchLogs();
        setInterval(() => {
            if ((process.env.REACT_APP_ENV || "local") !== "prod") {
                console.info(`[LoggerService] => Dispatching logs, timer tick every ${process.env.REACT_APP_LOGGER_DISPATCH_LOGS_INTERVAL_MIN || "5"} minute(s)`);
            }
            this.dispatchLogs();
        }, (parseInt(process.env.REACT_APP_LOGGER_DISPATCH_LOGS_INTERVAL_MIN || "5") * 60 * 1000));
    }
}

