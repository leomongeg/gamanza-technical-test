const fs = require('fs');
const path = require('path');
const AWS = require('aws-sdk');
const semver = require('semver');
const mime = require('mime-types')

/**
 * Webpack Plugin to upload files to the DigitalOcean Spaces CDN
 */
class CDNUpload {
    /**
     *
     * @param options:
     *  - appName: Name of the application ex: my-awesome-app.
     *  - subPath: Intermediate path to upload your build files
     *  - env: Intermediate path to upload your build files
     *  - distDirName: Name of the directory to upload to the cdn.
     *  - bucket: Name of the bucket to upload the content.
     *  - url: Url of the CDN ex: sfo2.digitaloceanspaces.com
     *  - accessKeyId: DigitalOcean Spaces accessKeyId
     *  - secretAccessKey: DigitalOcean Spaces secretAccessKey
     */
    constructor(options) {
        const spacesEndpoint = new AWS.Endpoint(options.url);
        this.s3 = new AWS.S3({
                                 endpoint: spacesEndpoint,
                                 accessKeyId: options.accessKeyId,
                                 secretAccessKey: options.secretAccessKey
                             });

        this.bucket = options.bucket;
        this.subPath = options.subPath;
        this.env = options.env;
        this.absolutePath = path.resolve("./");
        this.resourcesPath = `${this.absolutePath}/${options.distDirName}`;
        this.appName = options.appName;
    }

    apply(compiler) {
        compiler.hooks["afterEmit"].tap("CDNUpload", () => {
            this.listFiles();
            this.deleteFromCDN();
        });
    }

    /**
     * List al the files of the dist directory and process the upload to the CDN
     */
    listFiles() {
        const walk = function (dir, done) {
            let results = [];
            fs.readdir(dir, function (err, list) {
                if (err) return done(err);
                let pending = list.length;
                if (!pending) return done(null, results);
                list.forEach(function (file) {
                    file = path.resolve(dir, file);
                    fs.stat(file, function (err, stat) {
                        if (stat && stat.isDirectory()) {
                            walk(file, function (err, res) {
                                results = results.concat(res);
                                if (!--pending) done(null, results);
                            });
                        } else {
                            results.push(file);
                            if (!--pending) done(null, results);
                        }
                    });
                });
            });
        };
        walk(this.resourcesPath, (error, results) => {
            if (error) {
                console.error(error);
                return;
            }
            this.uploadToCDN(results);
        })
    }

    uploadToCDN(assets) {
        assets.forEach((item) => {
            const itemPath = item.replace(`${this.resourcesPath}/`, "");
            const readStream = fs.createReadStream(item, {highWaterMark: 16});
            const params = {
                Bucket: this.bucket,
                Key: `${this.appName}/${this.env}/${this.subPath}/${itemPath}`,
                Body: readStream,
                ACL: "public-read",
                ContentEncoding: 'gzip', // important
                CacheControl: 'max-age=31536000', // optional
                ContentType: mime.contentType(path.extname(item))
            };
            console.info(`[Uploading] -> ${item}`);
            this.s3.putObject(params, function (err, data) {
                if (err) {
                    console.log(`Error uploading ${itemPath}`);
                    console.error(err, err.stack);
                }
            });
        });
    }

    /**
     * Delete all the old files from the CDN.
     */
    deleteFromCDN() {
        console.info(`Cleaning old CDN Files for ${this.appName}:`);
        console.info('')
        this.s3.listObjects({
                                Bucket: this.bucket,
                                Marker: this.appName,
                                Prefix: `${this.appName}/${this.env}`
                            }, (err, data) => {
            if (err) {
                console.log(`Error listing CDN Objects for ${this.appName}`);
                console.error(err, err.stack);
            } else {
                const toDelete = [];
                data['Contents'].forEach((obj) => {
                    const parts = obj['Key'].split("/");
                    if (parts[0] !== this.appName || parts[1] !== this.env || parts[2] === this.subPath) return;
                    const versionInfo = semver.coerce(parts[2]);
                    if (versionInfo) {
                        let container = toDelete.find(predicate => predicate.version === versionInfo.version);
                        if (!container) {
                            container = {
                                version: versionInfo.version,
                                files: []
                            }
                            toDelete.push(container);
                        }
                        container.files.push(obj['Key']);
                        return;
                    }
                    this._cdnRemoveItem(obj['Key']);
                });

                // Order the versions to keep the most recent
                toDelete.sort((a, b) => semver.gt(a.version, b.version) ? -1 : 1);

                if (toDelete.length > 1) { // Keep only the last two recent versions
                    toDelete.shift();
                    toDelete.forEach(container => {
                        container.files.forEach(file => {
                            this._cdnRemoveItem(file);
                        })
                    });
                }
            }
        });
    }

    _cdnRemoveItem(key) {
        console.info(`[Removing] -> ${key}`);
        this.s3.deleteObject({
                                 Bucket: this.bucket,
                                 Key: key
                             }, function (err, data) {
            if (err) {
                console.log(`Error removing ${key}`);
                console.error(err, err.stack);
            }
        });
    }
}

module.exports = CDNUpload;
