const {getClientEnvironment} = require('../env');
const CDNUpload = require('./plugins/CDNUpload');

const cdn = new CDNUpload({
                              appName: "my-web-app",
                              distDirName: "dist",
                              subPath: process.env.REACT_APP_VERSION,
                              env: process.env.REACT_APP_ENV,
                              bucket: "bucket-assets",
                              url: process.env.DO_SPACES_URL,
                              accessKeyId: process.env.DO_SPACES_API_KEY,
                              secretAccessKey: process.env.DO_SPACES_API_SECRET
                          });


cdn.listFiles();
cdn.deleteFromCDN();