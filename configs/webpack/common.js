// shared config (dev and prod)
const {resolve} = require('path');
const {getClientEnvironment} = require('../env');
const {appNodeModules, appTsConfig} = require('../paths');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin');
const webpack = require('webpack');
// Webpack uses `publicPath` to determine where the app is being served from.
// In development, we always serve from the root. This makes config easier.
const publicPath = '/';
// `publicUrl` is just like `publicPath`, but we will provide it to our app
// as %PUBLIC_URL% in `index.html` and `process.env.PUBLIC_URL` in JavaScript.
// Omit trailing slash as %PUBLIC_PATH%/xyz looks better than %PUBLIC_PATH%xyz.
const publicUrl = '';

const env = getClientEnvironment(publicUrl);
// console.log(env.raw);

module.exports = {
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    context: resolve(__dirname, '../../src'),
    module: {
        rules: [
            {
                test: /\.js$/,
                // use: ['source-map-loader', 'babel-loader'],
                use: ['source-map-loader', {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env', {
                            "targets": "> 0.25%, not dead"
                        }]
                    },
                }],
                exclude: /node_modules/,
            },
            {
                test: /\.js$/,
                include: /node_modules\/@iconscout/,
                loader: "babel-loader",
                options: {
                    "presets": [
                        [
                            "@babel/preset-env",
                            {
                                "targets": "> 0.25%, not dead"
                            }
                        ]
                    ]
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', {loader: 'css-loader', options: {importLoaders: 1}}],
            },
            {
                test: /\.scss$/,
                loaders: [
                    'style-loader',
                    {loader: 'css-loader', options: {importLoaders: 1}},
                    {
                        loader: 'sass-loader',
                        options: {
                                prependData: `$cdn-url: "${process.env.REACT_APP_CDN_URL || ''}";`
                        }
                    },
                ],
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file-loader?hash=sha512&digest=hex&name=img/[hash].[ext]',
                    'image-webpack-loader?bypassOnDebug&optipng.optimizationLevel=7&gifsicle.interlaced=false',
                ],
            },
        ],
    },
    plugins: [
        new ForkTsCheckerWebpackPlugin({
                                           tsconfig: appTsConfig,
                                           reportFiles: ['src/**/*.{ts,tsx}', '!src/skip.ts']
                                       }),
        new InterpolateHtmlPlugin(HtmlWebpackPlugin, env.raw),
        new webpack.DefinePlugin(env.stringified),
    ],
    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM',
    },
    performance: {
        hints: false,
    },
};
